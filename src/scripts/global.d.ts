import { App } from './app';

declare module 'simple-scrollbar' {
    export namespace SimpleScrollbar {
        export function initEl(el: HTMLElement): void;
    }
    export default SimpleScrollbar;
}

declare global {
    interface Window { app: App; }
}
