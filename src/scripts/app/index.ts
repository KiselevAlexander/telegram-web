import { Auth } from '../auth';

class App {
    isAuth: boolean = false;
    auth: Auth;

    constructor() {
        if (!this.isAuth) {
            this.auth = new Auth();
        }
    }
}

export {
    App,
};
