/**
 * Парсинг JSON
 * @param value
 * @returns {null|any}
 */
export default (value: string | null) => {
  if (!value) {
    return null;
  }
  try {
    return JSON.parse(value);
  } catch (e) {
    return null;
  }
};
