import jsonParser from './jsonParser';


export const CACHE_PREFIX = 'tgweb';

/**
 * Cache manager
 * uses localStorage
 * @singleton
 */
const Storage = {
  /**
   * save resource in local storage
   * create unlimited cache if {lifeTime} is missing
   * @param {string} name - unique name of resource
   * @param {*} data - resource
   * @param {?number} [lifeTime] - per milliseconds
   * @param {?string} [flag] - resources group name
   */
  set(name: string, data: any, lifeTime?: number, flag?: string) {
    const expireDate = lifeTime ? new Date(Date.now() + lifeTime) : null;

    localStorage.setItem(CACHE_PREFIX + name, JSON.stringify({
      expireDate,
      flag,
      data,
    }));
  },

  /**
   * get resource by name
   * @param {string} name - unique name of resource
   * @return {*} resource
   */
  get(name: string) {
    const item = jsonParser(localStorage.getItem(CACHE_PREFIX + name));

    if (!(item instanceof Object)) {
      return null;
    }

    if (!item.expireDate) {
      return item.data;
    }

    const expireDate = new Date(item.expireDate);
    const now = new Date();

    if (expireDate > now) {
      return item.data;
    }

    Storage.remove(name);

    return null;
  },

  /**
   * remove resource by name
   * @param {string} name - unique name of resource
   */
  remove(name: string) {
    localStorage.removeItem(CACHE_PREFIX + name);
  },

  /**
   * remove all cached resources
   */
  removeAll() {
    Object.keys(localStorage).forEach((key) => {
      if (key.startsWith(CACHE_PREFIX)) {
        localStorage.removeItem(key);
      }
    });
  },

  /**
   * subscribe on changes of resource in local storage
   * @param {string} name - unique name of resource
   * @param {function(StorageEvent)} listener
   */
  subscribe(name: string, listener: (e: any) => any) {
    window.addEventListener('storage', (event) => {
      if (event.key === CACHE_PREFIX + name) {
        listener(event);
      }
    });
  },

  clearExpired() {
    Object.keys(localStorage)
      .forEach((name) => {
        const item = jsonParser(localStorage.getItem(name));

        if (item && name.startsWith(CACHE_PREFIX) && item.expireDate !== null) {
          const expireDate = new Date(item.expireDate);
          const now = new Date();


          if (expireDate < now) {
            localStorage.removeItem(name);
          }
        }
      });
  },
};

export default Storage;
