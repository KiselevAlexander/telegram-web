import { IInputKeyboardEvent, Input } from '../common/input';
import { Countries, ICountryFlag } from '../config';
import { Component } from '../common/component';
import { Button } from '../common/button';

enum AuthState {
    LOGIN='login',
    CODE='code',
    PASSWORD='password',
}

class Auth {
    state: AuthState = AuthState.LOGIN;

    states: object;

    private form: Component;

    private phone: Input<any>;
    private country: Input<ICountryFlag>;
    private next: Button;

    constructor() {
        this.form = new Component(<HTMLElement>document.querySelector('#authForm'));
        this.phone = new Input(document.querySelector('#phone'));
        this.country = new Input<ICountryFlag>(
            document.querySelector('#country'), {
                autocomplete: true,
                autocompleteData: Countries,
                autocompleteRender: (item) => {
                    return (
                        '<span class="country-flag">' + item.flag + '</span><span class="country-name">' + item.name + '</span><span class="country-code">' + item.codes[0] + '</span>'
                    );
                },
                autocompleteFilter: (value, item) => item.name.toUpperCase().startsWith(value.toUpperCase()),
                autocompleteRenderValue: (item) => item.name,
            }
        );
        this.country.listen('pick', this.countryChangeHandler);
        this.phone.listen('input', this.phoneChangeHandler);
        this.form.listen('submit', this.formSubmitHandler);

        this.next = new Button(document.querySelector('#loginBtn'));
        this.next.hide();
    }

    initStates() {
        this.states = {
            [AuthState.LOGIN]: new Component(<HTMLElement>document.querySelector('.auth-form-login')),
            [AuthState.CODE]: new Component(<HTMLElement>document.querySelector('.auth-form-code')),
            [AuthState.PASSWORD]: new Component(<HTMLElement>document.querySelector('.auth-form-password')),
        }
    }

    countryChangeHandler = () => {
        this.phone.val(this.country.autocompleteValue.codes[0]);
        this.phone.focus();
    };

    phoneChangeHandler = (e: IInputKeyboardEvent) => {
        // TODO: Validate phone format
        if (e.target.value.length > 5) {
            this.next.show();
        } else {
            this.next.hide();
        }
    };

    formSubmitHandler = (e: Event) => {
        e.preventDefault();
        console.log('Change state');
    };


}

export {
    Auth,
};
