interface IButtonClickEvent extends MouseEvent {
    target: HTMLButtonElement;
}

const Ripple = (el: HTMLButtonElement) => {
    el.addEventListener('mousedown', (e: IButtonClickEvent) => {
        let X = e.pageX - e.target.offsetLeft;
        let Y = e.pageY - e.target.offsetTop;
        let rippleDiv = document.createElement("span");
        rippleDiv.classList.add('ripple-effect');
        rippleDiv.setAttribute("style","top:"+Y+"px; left:"+X+"px;");
        let customColor = e.target.getAttribute('ripple-color');
        if(customColor) rippleDiv.style.background = customColor;
        e.target.appendChild(rippleDiv);
        rippleDiv.addEventListener('animationend', () => {
            rippleDiv.remove();
        });
    })
};

export {
    Ripple,
}

