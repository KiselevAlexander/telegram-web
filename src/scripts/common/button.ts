import { Component } from './component';

class Button extends Component {
    el: HTMLButtonElement;

    constructor(el: HTMLElement) {
        super(el);
    }

    loading(state: boolean) {
        if (state) {
            this.addClass('-loading');
        } else {
            this.removeClass('-loading');
        }
    }
}

export {
    Button,
};
