import { Component } from './component';

type TAutocompleteFilter<T> = (value: string, item: T) => boolean;

interface IInputKeyboardEvent extends KeyboardEvent {
    target: HTMLInputElement;
}

interface IInputConfig<T> {
    autocomplete?: boolean;
    autocompleteData?: T[];
    autocompleteRender?: (item: T) => string;
    autocompleteFilter?: TAutocompleteFilter<T>;
    autocompleteRenderValue?: (item: T) => string;
}

class Input<T> extends Component {
    private wrapper: Component;
    private listWrapper: Component;
    private autocompleteFilter: TAutocompleteFilter<T>;
    private itemsShowed: boolean = false;
    autocompleteValue: T;
    private selectedItem: number = -1;

    items: Component[] = [];
    filteredItems: Component[] = [];

    constructor(readonly el: HTMLInputElement, private readonly options: IInputConfig<T> = {}) {
        super(el);
        this.wrapper = new Component(el.parentElement);
        this.autocompleteFilter = options.autocompleteFilter;
        this.listen('input', this.changeHandler);

        if (options.autocomplete) {
            this.setAutocomplete(options.autocompleteData, options.autocompleteRender);
        }
    }

    private changeHandler = (e: IInputKeyboardEvent) => {
        if (e.target.value) {
            this.wrapper.addClass('-not-empty');
        } else {
            this.wrapper.removeClass('-not-empty');
        }
    };

    private showItems() {
        this.listWrapper.addClass('-showed');
        this.itemsShowed = true;
    }

    private hideItems() {
        this.listWrapper.removeClass('-showed');
        this.itemsShowed = false;
    }

    private selectItem(item: Component) {
        if (item) {
            item.addClass('-selected').el.scrollIntoView({
                block: "nearest"
            });
        }
        this.autocompleteValue = item ? item.meta : null;
    }

    private itemSelectHandler = (item: Component) => (e: Event) => {
        e.preventDefault();
        e.stopPropagation();
        this.selectItem(item);
        this.val(this.options.autocompleteRenderValue(item.meta));
        this.hideItems();
        this.emit('pick');
        console.log('handler');
    };

    private setAutocomplete(data: T[], renderer: (item: T) => string) {
        this.wrapper.addClass('autocomplete');

        const caret = new Component("DIV");
        caret.addClass('autocomplete-caret');
        caret.html('<svg><use xlink:href="#iconCaret"/></svg>');
        caret.listen('click', () => {
            if (!this.itemsShowed) {
                this.el.focus();
            }
        });

        this.wrapper.append(caret);

        this.listWrapper = new Component("DIV");
        this.listWrapper.addClass("autocomplete-items-wrapper");
        this.listWrapper.addClass("popup");
        const list = new Component("DIV");
        list.addClass("autocomplete-items");
        this.listWrapper.append(list);

        data.forEach((dataItem) => {
            const item = new Component('DIV', dataItem);
            item.html(renderer(dataItem));
            list.append(item);
            item.listen('mousedown', this.itemSelectHandler(item));
            this.items.push(item);
        });

        this.filteredItems = this.items;

        this.wrapper.append(this.listWrapper);

        this.listen('focus', () => {
            this.el.select();
            this.showItems();
        });

        this.listen('blur', () => {
            this.hideItems();
        });

        this.listen('input', (e: IInputKeyboardEvent) => {
            this.filteredItems = this.items.filter((i) => this.autocompleteFilter(e.target.value, i.meta));

            this.items.forEach((item) => {
                item.removeClass('-selected');
                if (!this.filteredItems.includes(item)) {
                    item.addClass('-hidden');
                } else {
                    item.removeClass('-hidden');
                }
            });
            this.selectedItem = 0;

            this.selectItem(this.filteredItems[0]);
        });

        this.listen('keydown', (e: KeyboardEvent) => {
            const current = this.filteredItems.find((item) => item.hasClass('-selected'));
            current && current.removeClass('-selected');

            if (e.key === 'ArrowDown') {
                if (this.selectedItem + 1 < this.filteredItems.length) {
                    this.selectedItem += 1;
                } else {
                    this.selectedItem = 0;
                }
                this.selectItem(this.filteredItems[this.selectedItem]);
            }

            if (e.key === 'ArrowUp') {
                if (this.selectedItem > 0) {
                    this.selectedItem -= 1;
                } else {
                    this.selectedItem = this.filteredItems.length - 1;
                }

                this.selectItem(this.filteredItems[this.selectedItem]);
            }

            if (e.key === 'Enter') {
                this.itemSelectHandler(this.filteredItems[this.selectedItem])(e);
            }
        });
    }

    val(value?: string): string {
        if (!value) {
            return this.el.value;
        }
        this.el.value = value;
        this.emit('input');
    }
}

export {
    Input,
    IInputConfig,
    IInputKeyboardEvent,
};
