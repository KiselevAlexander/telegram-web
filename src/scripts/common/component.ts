
class Component {
    el: HTMLElement;
    meta: any;

    constructor(el: HTMLElement | string, meta?: any) {
        this.el = (el instanceof Node) ? el : document.createElement("DIV");
        this.meta = meta;
    }

    hasClass(name: string) {
        return this.el.classList.contains(name);
    }

    addClass(name: string) {
        this.el.classList.add(name);
        return this;
    }

    removeClass(name: string) {
        this.el.classList.remove(name);
        return this;
    }

    listen(e: string, callback: (e: Event) => void) {
        this.el.addEventListener(e, callback);
        return this;
    }

    emit(eventName: string) {
        this.el.dispatchEvent(new Event(eventName));
        return this;
    }

    html(v: string) {
        this.el.innerHTML = v;
        return this;
    }

    text(v: string) {
        this.el.innerText = v;
        return this;
    }

    append(child: HTMLElement | Component) {
        this.el.appendChild((child instanceof Node) ? child : child.el);
        return this;
    }

    next() {
        return this.el.nextSibling ? new Component(<HTMLScriptElement>this.el.nextSibling) : null;
    }

    prev() {
        return this.el.previousSibling ? new Component(<HTMLScriptElement>this.el.previousSibling) : null;
    }

    focus() {
        this.el.focus();
        return this;
    }

    blur() {
        this.el.blur();
        return this;
    }

    attr(name: string, value?: string): string {
        if (!value) {
            return this.el.getAttribute(value);
        }
        this.el.setAttribute(name, value);
    }

    removeAttr(name: string) {
        this.el.removeAttribute(name);
    }

    hide() {
        this.attr('hidden', 'true');
    }

    show() {
        this.removeAttr('hidden');
    }

}

export {
    Component,
};
